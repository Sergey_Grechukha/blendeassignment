package com.loki.blende.di

import com.loki.blende.di.sub_comonents.SubModuleFactory
import com.nhaarman.mockito_kotlin.mock
import dagger.Module

@Module
class TestAppModule: AppModule(){

    override fun getModuleFactory(): SubModuleFactory {
        return mock()
    }
}