package com.loki.blende.rules

import android.content.Context

import com.loki.blende.BlendeApp
import com.loki.blende.di.AppComponent
import com.loki.blende.di.DaggerTestAppComponent
import com.loki.blende.di.TestAppModule

import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement


class DaggerRule(private val mContext: Context) : TestRule {

    private val mTestComponent: AppComponent

    init {
        mContext.applicationContext as BlendeApp
        mTestComponent = DaggerTestAppComponent
                .builder()
                .testAppModule(TestAppModule()).build()
    }


    override fun apply(base: Statement, description: Description): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                val application = mContext.applicationContext as BlendeApp
                // Set the TestComponent before the waiting_animator runs
                application.appComponent = mTestComponent
                base.evaluate()
            }
        }
    }
}
