package com.loki.blende.model.entity

data class Gallery(val amountOfImagesInGallery: Int, val images: List<GalleryImage>)