package com.loki.blende.model.entity

data class GalleryImage(val previewUrl: String, val fullImage: String)