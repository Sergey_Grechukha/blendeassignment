package com.loki.blende.di

import com.loki.blende.di.sub_comonents.SubModuleFactory
import com.loki.blende.model.repository.ImageProvider
import com.loki.blende.model.repository.pixabay.PixaBayImageProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class AppModule {

    @Provides
    @Singleton
    fun imageProvider(): ImageProvider {
        return PixaBayImageProvider()
    }

    @Provides
    @Singleton
    fun provideSubModuleFactory(): SubModuleFactory {
        return getModuleFactory()
    }

    open fun getModuleFactory() = SubModuleFactory()
}