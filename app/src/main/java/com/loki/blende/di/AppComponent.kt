package com.loki.blende.di

import com.loki.blende.di.sub_comonents.GallerySubComponent
import com.loki.blende.di.sub_comonents.GallerySubModule
import com.loki.blende.di.sub_comonents.SubModuleFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent{

    fun getSubModuleFactory(): SubModuleFactory
    fun plus(subModule: GallerySubModule): GallerySubComponent
}