package com.loki.blende.di.sub_comonents

import com.loki.blende.ui.controllers.GridGalleryController

open class SubModuleFactory {

    open fun getModule(controller: GridGalleryController) = GallerySubModule(controller)
}