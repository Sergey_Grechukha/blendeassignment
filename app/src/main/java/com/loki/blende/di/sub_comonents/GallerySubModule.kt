package com.loki.blende.di.sub_comonents

import com.loki.blende.model.repository.ImageProvider
import com.loki.blende.contract.GalleryContract
import com.loki.blende.presenters.GalleryPresenter
import dagger.Module
import dagger.Provides

@Module
open class GallerySubModule(val view: GalleryContract.View) {

    @Provides
    open fun providePresenter(imageProvider: ImageProvider): GalleryContract.Presenter {
        return GalleryPresenter(imageProvider, view)
    }
}