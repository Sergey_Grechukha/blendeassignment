package com.loki.blende.di.sub_comonents

import com.loki.blende.ui.controllers.GridGalleryController
import dagger.Subcomponent

@Subcomponent(modules = [(GallerySubModule::class)])
interface GallerySubComponent{

    fun inject(view: GridGalleryController)
}