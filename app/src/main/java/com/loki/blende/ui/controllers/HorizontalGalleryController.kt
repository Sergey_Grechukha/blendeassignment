package com.loki.blende.ui.controllers

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.loki.blende.R
import com.loki.blende.databinding.ControllerGalleryBinding
import com.loki.blende.ui.adapter.GalleryAdapter


class HorizontalGalleryController() : Controller() {

    private var position: Int = 0

    constructor(images: List<String>, position: Int) :
            this(Bundle()) {
        adapter.updateOrClear(images)
        this.position = position
    }

    constructor(args: Bundle) : this()

    private lateinit var binding: ControllerGalleryBinding

    private var adapter = GalleryAdapter(true)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.controller_gallery, container, false)
        setupView()
        return binding.root
    }

    private fun setupView() {
        val snapHelper = PagerSnapHelper()
        binding.rvGallery.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        snapHelper.attachToRecyclerView(binding.rvGallery)
        binding.rvGallery.adapter = adapter
        binding.rvGallery.setItemViewCacheSize(6)
        binding.rvGallery.scrollToPosition(position)
    }



}