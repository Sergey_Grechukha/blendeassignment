package com.loki.blende.ui.utils

import android.app.Activity
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import kotlin.reflect.KProperty

class ActivityBindingDelegate<out T : ViewDataBinding>(@LayoutRes private val layoutId: Int) {

    private var value: T? = null

    operator fun getValue(activity: Activity, property: KProperty<*>): T {
        if (value == null) {
            value = DataBindingUtil.setContentView(activity, layoutId)
        }
        return value!!
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: ViewDataBinding){}
}