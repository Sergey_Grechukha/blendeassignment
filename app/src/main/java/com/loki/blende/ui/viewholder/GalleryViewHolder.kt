package com.loki.blende.ui.viewholder

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import com.loki.blende.databinding.ItemGalleryImageBinding


open class GalleryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding: ItemGalleryImageBinding by lazy {
        DataBindingUtil.bind<ItemGalleryImageBinding>(itemView)
    }

    fun setData(data: String, withDynamicBackGround: Boolean) {
        binding.imageUrl = data
        binding.dynamicBackGround = withDynamicBackGround
    }
}


