package com.loki.blende.ui.utils

import android.databinding.BindingAdapter
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.loki.blende.R

@BindingAdapter(value = ["imageUrl", "withDynamicBackGround"], requireAll = false)
fun fetchImageByUrl(view: ImageView, imageUrl: String, withDynamicBackGround: Boolean) {

    val pb = (view.parent as ViewGroup).findViewById<ProgressBar>(R.id.pb_loading)
    pb.visibility = View.VISIBLE
    val requestListener: RequestListener<Drawable?> = object : RequestListener<Drawable?> {
        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable?>?, isFirstResource: Boolean): Boolean {
            pb.visibility = View.GONE
            return false
        }

        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable?>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
            pb.visibility = View.GONE
            setBackgroundColor(resource)
            return false
        }

        private fun setBackgroundColor(resource: Drawable?) {
            if (withDynamicBackGround && resource != null) {
                val color = calculateAverageColor(resource, 3)
                try {
                    val recycler = view.parent.parent as View
                    (recycler.parent as View).setBackgroundColor(color)
                    recycler.setBackgroundColor(color)
                } catch (error: Exception) {
                    Log.e("Binding", error.message)
                }

            }
        }
    }
    Glide
            .with(view)
            .load(Uri.parse(imageUrl))
            .thumbnail(0.5f)
            .listener(requestListener)
            .into(view)

}

/**
 * pixelSpacing tells how many pixels to skip each pixel.
 * If pixelSpacing > 1: the average color is an estimate, but higher values mean better performance
 * If pixelSpacing == 1: the average color will be the real average
 * If pixelSpacing < 1: the method will most likely crash (don't use values below 1)
 */
fun calculateAverageColor(drawable: Drawable, pixelSpacing: Int): Int {
    var R = 0
    var G = 0
    var B = 0

    val bitmap = (drawable as BitmapDrawable).bitmap

    val height = bitmap.height
    val width = bitmap.width
    var n = 0
    val pixels = IntArray(width * height)
    bitmap.getPixels(pixels, 0, width, 0, 0, width, height)
    var i = 0
    while (i < pixels.size) {
        val color = pixels[i]
        R += Color.red(color)
        G += Color.green(color)
        B += Color.blue(color)
        n++
        i += pixelSpacing
    }
    return Color.rgb(R / n, G / n, B / n)
}