package com.loki.blende.ui.controllers

import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.databinding.DataBindingUtil
import android.graphics.Point
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.loki.blende.BlendeApp
import com.loki.blende.R
import com.loki.blende.databinding.ControllerGalleryBinding
import com.loki.blende.contract.GalleryContract
import com.loki.blende.ui.adapter.GalleryAdapter
import com.loki.blende.ui.utils.EndlessScrollListener
import com.loki.blende.ui.utils.OnLoadMoreListener
import com.loki.blende.ui.utils.RecyclerItemClickListener
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject
import kotlin.properties.Delegates


class GridGalleryController : Controller(), GalleryContract.View, OnLoadMoreListener {

    companion object {
        private const val PORTRAIT_SPAN_COUNT: Int = 2
        private const val LANDSCAPE_SPAN_COUNT: Int = 3
    }

    @Inject
    lateinit var presenter: GalleryContract.Presenter
    private val component by lazy {
        val appComponent = (applicationContext as BlendeApp).appComponent
        appComponent.plus(appComponent.getSubModuleFactory().getModule(this)) }
    private var binding: ControllerGalleryBinding by Delegates.notNull()

    private val adapter = GalleryAdapter()
    private var isNew = true
    private var currentFirstItem: IntArray? = IntArray(PORTRAIT_SPAN_COUNT)
    private var rvSpanCount: Int = PORTRAIT_SPAN_COUNT
    private val scrollListener = EndlessScrollListener(IntArray(PORTRAIT_SPAN_COUNT), this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.controller_gallery, container, false)
        setUpView()
        return binding.root
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        if (isNew) {
            component.inject(this)
            presenter.onResume()
            fetchImages()
            isNew = false
        }
    }

    private fun setUpView() {
        rvSpanCount = getSpanCount()
        binding.rvGallery.layoutManager = StaggeredGridLayoutManager(rvSpanCount, StaggeredGridLayoutManager.VERTICAL)
        binding.rvGallery.adapter = adapter
        binding.rvGallery.setItemViewCacheSize(6)
        scrollRvToPreviousPosition()
        binding.rvGallery.addOnScrollListener(scrollListener)
        binding.rvGallery.addOnItemTouchListener(RecyclerItemClickListener(applicationContext!!,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        router.pushController(RouterTransaction
                                .with(HorizontalGalleryController(presenter.getFullImages(), position))
                                .pushChangeHandler(FadeChangeHandler())
                                .popChangeHandler(FadeChangeHandler()))
                    }

                    override fun onItemLongClick(childView: View, position: Int, point: Point) {
                        //no-op
                    }
                }))
    }

    private fun getSpanCount(): Int {
        val spanCount = if (activity?.resources?.configuration?.orientation == ORIENTATION_PORTRAIT) {
            PORTRAIT_SPAN_COUNT
        } else {
            LANDSCAPE_SPAN_COUNT
        }
        scrollListener.currentFirstItem = IntArray(spanCount)
        return spanCount
    }

    private fun scrollRvToPreviousPosition() {
        val position = scrollListener.getCurrentFirstItemPosition()
        if (position != 0) {
            (binding.rvGallery.layoutManager as StaggeredGridLayoutManager).scrollToPositionWithOffset(position, 0)
        }
        currentFirstItem = IntArray(rvSpanCount)
    }

    private fun fetchImages() {
        presenter.subscribeForNewImages()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onNewImages(it) }
        presenter.getImages()
    }

    private fun onNewImages(imageUrls: List<String>) {
        adapter.updateOrClear(imageUrls)
    }

    override fun onLoadMore() {
        presenter.getNextImages()
    }

    override fun onError(error: Throwable) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onPause()
    }
}