package com.loki.blende.ui.utils

import android.content.Context
import android.graphics.Point
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

class RecyclerItemClickListener(context: Context, private val listener: OnItemClickListener) :
        RecyclerView.OnItemTouchListener {

    private var recyclerView: RecyclerView? = null

    private var gestureDetector: GestureDetector

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)

        fun onItemLongClick(childView: View, position: Int, point: Point)
    }

    init {
        gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent): Boolean {

                return true
            }

            override fun onLongPress(e: MotionEvent) {
                val childView = recyclerView!!.findChildViewUnder(e.x, e.y)
                listener.onItemLongClick(childView, recyclerView!!.getChildAdapterPosition(childView),
                        Point(e.x.toInt(), e.y.toInt()))

            }
        })
    }

    override fun onInterceptTouchEvent(view: RecyclerView, e: MotionEvent): Boolean {
        this.recyclerView = view
        val childView = view.findChildViewUnder(e.x, e.y)
        if (childView != null && gestureDetector.onTouchEvent(e)) {
            listener.onItemClick(childView, view.getChildAdapterPosition(childView))
            return true
        }
        return false
    }

    override fun onTouchEvent(view: RecyclerView, motionEvent: MotionEvent) {}

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

    }
}