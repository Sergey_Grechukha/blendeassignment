package com.loki.blende.ui.utils

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager

class EndlessScrollListener(
        var currentFirstItem: IntArray?,
        private val listener: OnLoadMoreListener): RecyclerView.OnScrollListener(){

    private var lastVisibleItem = 2

    private val visibleThreshold = 10

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val staggeredGridLayoutManager = recyclerView?.layoutManager as StaggeredGridLayoutManager
        staggeredGridLayoutManager
                .findFirstCompletelyVisibleItemPositions(currentFirstItem)

        val totalItemCount = staggeredGridLayoutManager.itemCount
        val lastVisibleItems = staggeredGridLayoutManager
                .findLastVisibleItemPositions(IntArray(staggeredGridLayoutManager.spanCount))


        when {
            staggeredGridLayoutManager.spanCount == 1 -> lastVisibleItem = lastVisibleItems[0]
            staggeredGridLayoutManager.spanCount == 2 -> lastVisibleItem = Math.max(lastVisibleItems[0],
                    lastVisibleItems[1])
            staggeredGridLayoutManager.spanCount == 3 -> lastVisibleItem =
                    Math.max(Math.max(lastVisibleItems[0], lastVisibleItems[1]), lastVisibleItems[2])
        }

        if (totalItemCount <= lastVisibleItem + visibleThreshold) {
            listener.onLoadMore()
        }
    }

    fun getCurrentFirstItemPosition() = currentFirstItem?.get(0) ?: 0
}
interface OnLoadMoreListener{
    fun onLoadMore()
}