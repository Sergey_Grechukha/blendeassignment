package com.loki.blende.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.loki.blende.R
import com.loki.blende.databinding.ActivityMainBinding
import com.loki.blende.ui.controllers.GridGalleryController
import com.loki.blende.ui.utils.ActivityBindingDelegate
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {

    private var contentRouter: Router by Delegates.notNull()
    private val binding by ActivityBindingDelegate<ActivityMainBinding>(R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        contentRouter = Conductor.attachRouter(this, binding.container, savedInstanceState)
        if (!contentRouter.hasRootController()) {
            contentRouter.setRoot(RouterTransaction.with(GridGalleryController()))
        }
    }

    override fun onBackPressed() {
        if (!contentRouter.handleBack()) {
            super.onBackPressed()
        }
    }
}
