package com.loki.blende

import android.app.Application
import com.loki.blende.di.AppComponent
import com.loki.blende.di.AppModule
import com.loki.blende.di.DaggerAppComponent
import kotlin.properties.Delegates

class BlendeApp : Application() {

    var appComponent: AppComponent by Delegates.notNull()

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder().appModule(AppModule()).build()
    }
}
